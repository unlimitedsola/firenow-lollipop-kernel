package com.android.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * @author lql
 *
 */
public class ScheduleOnOffReceiver extends BroadcastReceiver {

	/* (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(Context arg0, Intent data) {
		        Log.v("sjf",data.getAction());
                if(data.getAction().equals("android.timerswitch.run_power_off"))
                {
//                   Toast.makeText(arg0, "will power_off", Toast.LENGTH_SHORT).show();
                    Intent newIntent = new Intent(Intent.ACTION_REQUEST_SHUTDOWN);
                     newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                     arg0.startActivity(newIntent);
                }
	}
}