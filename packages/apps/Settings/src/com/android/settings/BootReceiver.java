package com.android.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;
import android.os.SystemProperties;

public class BootReceiver extends BroadcastReceiver{

       @Override
       public void onReceive(Context arg0, Intent arg1) {
               // TODO Auto-generated method stub
               
               String action = arg1.getAction();
               if(action.equals(Intent.ACTION_BOOT_COMPLETED))
               { 
               SharedPreferences shared = arg0.getSharedPreferences("com.android.settings_preferences", Context.MODE_PRIVATE);
               boolean enable_log_save = shared.getBoolean("enable_log_save", false);
               if(enable_log_save)
                       SystemProperties.set("app.logsave.start","1");
                       
               }
       }

}
